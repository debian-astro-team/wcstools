#!/bin/bash

exitval=0
lastProg=""

testProg () {
    prog=$(basename $1)
    expected=${prog}.expected
    logfile=${prog}.result
    cat > $expected
    if [ "$prog" != "$lastprog" ] ; then
	echo ; echo -n "$prog\t"
	lastprog=$prog
    fi
    $* > $logfile
    r=$?
    if [ "$r" != 0 ] ; then
	echo -n E
	cat >> log <<EOF
------------------------------------------------------------------------
  $*
 has exit value "$r"
EOF
	exitval=$r
    fi
    if ! diff -qwB "$expected" "$logfile" > /dev/null ; then
	if [ "$r" = 0 ] ; then
	cat >> log <<EOF
------------------------------------------------------------------------
  $*
EOF
	fi
	echo -n F
	cat >> log <<EOF

expected:
$(cat $expected)

result:
$(cat $logfile)

diff:
$(diff -U0 -wB "$expected" "$logfile")
EOF
	exitval=1
    else
	echo -n .
	rm -f "$expected" "$logfile"
    fi
    return 0
}

test_dir="$AUTOPKGTEST_TMP"
if [ -z "$test_dir" ] ; then
    test_dir="$(mktemp -d)"
fi

cp -f libwcs/xx.fits "$test_dir"
cd "$test_dir"
fitsfile="xx.fits"

cat <<EOF
Test summary
============
 .  success
 E  exit value != 0
 F  output not as expected
EOF

cleanup () {
    echo
    if [ $exitval = 0 ]; then
	cat <<EOF 

All tests OK
EOF
    fi
    if [ -f log ] ; then
	cat <<EOF

Detailed failure results
========================
EOF
	cat log
	cat <<EOF
------------------------------------------------------------------------
EOF
	rm -f log
    fi
    if [ "$test_dir" != "$AUTOPKGTEST_TMP" ] ; then
	rm -rf "$test_dir"
    fi
    exit $exitval
}

#
# Syntax for the tests is
#
# testProg command args <<EOF
# [expected output]
# EOF
#
# These examples were mainly taken from the wcstools home page,
# http://tdc-www.harvard.edu/software/wcstools/wcsprogsi.html
#

testProg xy2sky $fitsfile 1234 123411 <<EOF
12:25:34.091 +65:54:08.53 J2000 1234.000 123411.000
EOF

testProg xy2sky -b $fitsfile 1 100 200 <<EOF
12:25:13.047 +44:17:28.95 B1950    1.000  100.000 
EOF

testProg sky2xy  $fitsfile 12:27:33.179 +44:02:01.05 J2000 <<EOF
12:27:33.179 +44:02:01.05 J2000 ->  100.006  200.000
EOF

testProg sky2xy $fitsfile 149.10704 68.92327 J2000 320 -31 FK5 <<EOF
149.10704 68.92327 J2000 -> 79358.147 170585.857 (off image)
320 -31 FK5 ->    0.000    0.000 (offscale)
EOF

testProg  getpix $fitsfile 200 100 <<EOF
34154
EOF

testProg  getpix $fitsfile 100-102 200-203 <<EOF
33575 33526 33983 
33482 33567 33570 
33821 34074 33764 
33620 33951 33866 
EOF

testProg getpix -l 325 $fitsfile 0 0 <<EOF
[1040,175] = 323.000000
[1040,202] = 324.000000
[1040,239] = 324.000000
[1040,489] = 324.000000
[1040,937] = 324.000000
[1040,1024] = 324.000000
EOF

testProg gethead $fitsfile  ra dec <<EOF
12:28:10.960 +44:05:33.40
EOF

testProg imsize $fitsfile <<EOF
xx.fits 12:27:06.940 +44:05:31.15 J2000 11.687mx11.510m -0.6721/0.6721s/pix  1040x1025 pix
EOF

testProg imsize -d $fitsfile <<EOF
xx.fits 186.779 44.092 J2000 0.195x0.192 -0.6721/0.6721s/pix  1040x1025 pix
EOF

testProg imsize -c +1.0 $fitsfile  <<EOF
xx.fits 12:27:06.940 +44:05:31.15 J2000 71.687mx71.510m
EOF

testProg sumpix $fitsfile 0 1 <<EOF
31738876.00 
EOF

testProg sumpix $fitsfile 1-5 1-5 <<EOF
8788.000000 
EOF

testProg sumpix $fitsfile  <<EOF
31032679930.00
EOF

testProg imstar -n 2 $fitsfile <<EOF
  1 12:27:21.699 +44:11:02.52 -12.86   283.50 1005.72 65535
  2 12:26:57.521 +44:03:16.90 -11.71   671.90  313.53 45945
EOF

testProg skycoor 9:55:41.600 +69:00:27.99 J2000 9:54:53.258 +69:03:49.30 J2000 <<EOF
09:51:36.102 +69:14:41.51 B1950
09:50:46.961 +69:18:00.96 B1950
EOF

testProg skycoor 09:51:36.102 +69:14:41.51 B1950 09:50:46.961 +69:18:00.96 B1950 <<EOF
09:55:41.589 +69:00:28.17 J2000
09:54:53.246 +69:03:49.49 J2000
EOF

testProg skycoor -g -n 6 9:55:41.600 +69:00:27.99 J2000 9:54:53.258 +69:03:49.30 J2000 <<EOF
142.144474 40.943450 galactic 
142.138707 40.852337 galactic
EOF

testProg skycoor -j 142.144474 40.943450 galactic 142.138707 40.852337 galactic <<EOF
09:55:41.600 +69:00:27.99 J2000
09:54:53.258 +69:03:49.30 J2000
EOF

testProg skycoor -r 9:55:41.600 +69:00:27.99 9:54:53.258 +69:03:49.30 <<EOF
328.383
EOF

cleanup
