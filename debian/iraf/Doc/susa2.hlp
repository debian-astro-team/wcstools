.help susa2 Mar2001 WCSTools
.ih
NAME
susa2 -- Find USNO-SA2.0 Catalog stars in a square on the sky
.ih
USAGE
susac [options] [-b or -j] ra dec
.ih
ARGUMENTS
.ls -a
list single closest catalog source
.le
.ls -b <RA> <Dec>
Output B1950 (FK4) coordinates around this center
.le
.ls -d
Sort by distance from center instead of flux
.le
.ls -h
Print heading, else do not 
.le
.ls -j <RA> <Dec>
Output J2000 (FK5) coordinates around this center
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -o <name>
Object name used to name output file
.le
.ls -r <radius>
Search radius in arcsec (default 10)
.le
.ls -s
Sort by right ascension instead of flux 
.le
.ls -t
Tab table to standard output as well as file
.le
.ls -u <num>
Plate number for catalog sources (0=all)
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file imagename.uac
.le
.ih
DESCRIPTION
.I suac
finds all of the U.S. Naval Observatory SA2.0 Catalog objects in a specified
region of the sky and lists their sky positions and magnitudes in order of
brightness. Output is to standard out, unless the -w flag is set, in which
case it goes to objectname.uac or search.uac. It is a link to scat.
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/scat/susa2.html
.ih
SEE ALSO
scat, sua2, imusa2, sgsc
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
