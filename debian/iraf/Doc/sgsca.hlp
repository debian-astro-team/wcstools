.help sgsca Sep2001 WCSTools
.ih
NAME
sgsca -- Find GSC-ACT stars in a square on the sky
.ih
USAGE
sgsc [options] [-b or -j] ra dec
.ih
ARGUMENTS
.ls -a
List single closest catalog source
.le
.ls -b <RA> <Dec>
Output B1950 (FK4) coordinates
.le
.ls -d
Sort by distance from center instead of flux
.le
.ls -g <class>
Object class (0=stars 3=galaxies -1=all)
.le
.ls -h
Print heading, else do not 
.le
.ls -j <RA> <Dec>
Output J2000 (FK5) coordinates around this center
.le
.ls -m [<bright magnitude>] <faint magnitude>
Limiting catalog magnitude(s) (default none, bright -2 if only faint is given)
.le
.ls -n <num>
Number of brightest stars to print 
.le
.ls -o <name>
Object name used for output file naming
.le
.ls -r <radius>
Radius of search circle 1in arcsec (default 10)
.le
.ls -s
Sort by RA instead of flux 
.le
.ls -t
Tab table to standard output as well as file
.le
.ls -v
Verbose listing of processing intermediate results
.le
.ls -w
Write tab table output file imagename.gsc
.le
.ih
DESCRIPTION
.I sgsc
is a utility for finding all of the GSC-ACT Star
Catalog objects in a specified region of the sky and listing them with their
sky positions and magnitudes in order of brightness, with the brighter ones
first.
Output is to standard out, unless the -w flag is set, in
which case it goes to objectname.gsca or search.gsca.
It is a link to scat.
.ih
WEB PAGE
http://tdc-www.harvard.edu/software/wcstools/scat/sgsca.html
.ih
SEE ALSO
scat, imgsca, imcat
.ih
AUTHOR
Jessica Mink, SAO (jmink@cfa.harvard.edu)
.endhelp
